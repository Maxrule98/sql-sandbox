const Pool = require('pg').Pool
const pool = new Pool({
  host: 'localhost',
  port: '5432',
  user: 'postgres',
  password: 'postgres',
  database: 'meals'
});

const getVegetables = (request, response) => {
  pool.query('SELECT * FROM vegetables ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getVegetablesById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM vegetables WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

module.exports = {
  getVegetables,
  getVegetablesById
}