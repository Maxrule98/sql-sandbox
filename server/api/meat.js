const Pool = require('pg').Pool
const pool = new Pool({
  host: 'localhost',
  port: '5432',
  user: 'postgres',
  password: 'postgres',
  database: 'meals'
});

const getMeat = (request, response) => {
  pool.query('SELECT * FROM meat ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getMeatById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM meat WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

module.exports = {
  getMeat,
  getMeatById
}