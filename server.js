const Bundler = require("parcel-bundler");
const bodyParser = require('body-parser')
const express = require("express");

const app = express();

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

/*-------------------------------------------*/

  const meat = require('./server/api/meat.js')
  const veg = require('./server/api/vegetables.js')

/*-------------------------------------------*/
//ROUTES

app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

app.get('/meat', meat.getMeat)
app.get('/meat/:id', meat.getMeatById)

app.get('/vegetables', veg.getVegetables)
app.get('/vegetables/:id', veg.getVegetablesById)


/*-------------------------------------------*/

var server = app.listen(5000, function() {
  console.log("app running on http://localhost:" + server.address().port);
});